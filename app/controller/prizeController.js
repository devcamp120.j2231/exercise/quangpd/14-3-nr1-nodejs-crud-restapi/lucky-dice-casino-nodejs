//Import thư viện mongoose
const mongoose = require("mongoose");

//Import prizeModel 
const prizeModel  = require("../model/prizeModel");

//Create New Prizes
const createNewPrize = (req,res) =>{
    //B1 : thu thập dữ liệu
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!body.name){
        return res.status(400).json({
            message :`Bad request - name is required !`
        })
    }

    //B3 : Xử lý dữ liệu
    let newPrize = new prizeModel({
        _id : mongoose.Types.ObjectId(),
        name : body.name,
        description : body.description
    });

    prizeModel.create(newPrize,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Create New Prize Successfully !`,
                Prize : data
            })
        }
    })
}

//Get All Prizes
const getAllPrize = (req,res) =>{
    //B1 : thu thập dữ liệu
    //B2 : Kiểm tra dữ liệu
    //B3 : Xử lý và trả về dữ liệu
    prizeModel.find((err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message :`Get All Prize Successfully !`,
                Prizes : data
            })
        }
    })
}

//Get Prizes Id 
const getPrizeById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let prizeId = req.params.prizeId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeId)){
        return res.status(400).json({
            message : `Bad request - prizeId is not valid !`
        })
    }

    //B3 : Xử lý và trả về kết quả 
    prizeModel.findById(prizeId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Load Prize By Id Successfully !`,
                Prizes : data
            })
        }
    })
}

//Update Prizes Id
const updatePrizeById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let prizeId = req.params.prizeId;
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeId)){
        return res.status(400).json({
            message :`Bad request - prizeId is not valid !`
        })
    }

    if(!body.name){
        return res.status(400).json({
            message :`Bad request - name is required !`
        })
    }

    //B3: Xử lý và thu thập dữ liệu
    let updatePrize = new prizeModel({
       name : body.name,
       description : body.description
    });

    prizeModel.findByIdAndUpdate(prizeId,updatePrize,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :` Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Update Prize By Id Successfully !`,
                users : data
            })
        }
    })
}

//Delete Prizes By Id 
const deletePrizeById = (req,res) =>{
    //B1 : thu thập dư liệu
    let prizeId = req.params.prizeId;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeId)){
        return res.status(400).json({
            message : `Bad request - prizeId is not valid `
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    prizeModel.findByIdAndDelete(prizeId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(204).send()
        }
    })
}
//Export thành module 
module.exports = { createNewPrize ,getAllPrize , getPrizeById , updatePrizeById , deletePrizeById };