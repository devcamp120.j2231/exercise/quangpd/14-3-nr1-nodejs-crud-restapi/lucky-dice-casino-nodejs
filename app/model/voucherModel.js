//Import thư viện mongoose
const mongoose = require("mongoose");

//Khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

const voucherSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    code : {
        type : String,
        unique : true,
        required : true
    },
    discount :{
        type : Number,
        required : true 
    },
    note : {
        type : String,
        required : false
    },
    createdAt : {
        type : Date,
        default : Date.now()
    },
    updateAt : {
        type : Date,
        default : Date.now()
    }
})

//Export thành model 
module.exports = mongoose.model("voucher",voucherSchema);