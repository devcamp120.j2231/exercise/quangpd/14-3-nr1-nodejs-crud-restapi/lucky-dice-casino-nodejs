//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Khai báo thư viện Schema của mongoose 
const Schema = mongoose.Schema;

const userSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    username : {
        type : String,
        unique : true,
        required : true
    },
    firstname : {
        type : String,
        required : true 
    },
    lastname : {
        type : String,
        required : true
    },
    createdAt : {
        type : Date,
        default : Date.now()
    },
    updatedAt : {
        type : Date,
        default : Date.now()
    }
});

//Export thành model 
module.exports = mongoose.model("User",userSchema);