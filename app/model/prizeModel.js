//Import thư viện mongoose
const mongoose = require("mongoose");

//Khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

const prizeSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    name :{
        type : String,
        unique : true,
        required : true
    },
    description : {
        type : String,
        required : false
    },
    createdAt : {
        type : Date,
        default : Date.now()
    },
    updateAt : {
        type : Date,
        default : Date.now()
    }
})

//Export thành model
module.exports = mongoose.model("prize",prizeSchema);