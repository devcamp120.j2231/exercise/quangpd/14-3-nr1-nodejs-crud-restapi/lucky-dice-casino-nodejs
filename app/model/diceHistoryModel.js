//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Khai báo thư viện Schema của mongoose 
const Schema = mongoose.Schema;

const diceHistorySchema = new Schema({
    _id : mongoose.Types.ObjectId,
    user : {
        type : mongoose.Types.ObjectId,
        ref : 'User',
        required : true
    },
    createdAt : {
        type : Date,
        default : Date.now()
    },
    updatedAt : {
        type : Date,
        default : Date.now()
    }
});

//Export thành model
module.exports = mongoose.model("DiceHistory",diceHistorySchema);