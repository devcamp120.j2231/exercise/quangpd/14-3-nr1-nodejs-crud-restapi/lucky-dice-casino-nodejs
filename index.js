//import thư viện express
const express = require("express");

const path = require("path");

//Khai báo mongoose
const mongoose = require("mongoose");

//Khai báo các model mongoose
const userModel = require("./app/model/userModel");
const diceHistoryModel = require("./app/model/diceHistoryModel");
const prizeModel = require("./app/model/prizeModel");
const voucherModel = require("./app/model/voucherModel");

//Import router
const { userRouter } = require("./app/router/userRouter");
const { diceHistoryRouter } = require("./app/router/diceHistoryRouter");
const { prizeRouter } = require("./app/router/prizeRouter");
const { voucherRouter } = require("./app/router/voucherRouter");



//Khởi tạo app
const app = express();

//Khai báo cổng
const port = 8000;

//Khai báo sử dụng json
app.use(express.json());



//MidleWare Time And Method
const timeAndMethodMiddleWare = (req,res,next) =>{
    console.log("Time : " + new Date() + "Method : " + req.method );
    next();
}

app.use(timeAndMethodMiddleWare);

app.get("/random-number",(req,res) =>{
    let randomNumber = Math.floor(Math.random() * 7);

    return res.status(200).json({
        message : `Your Number is random !`,
        number : randomNumber
    })
});

app.get("/",(req,res) =>{
    console.log(__dirname);

    res.sendFile(path.join(__dirname + "/views/luckydicecasino/luckydiceCasino.html"));
});

app.use(express.static(__dirname + "/views/luckydicecasino"));
//Kết nối cơ sở dữ liệu 
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_LuckyDice",(error) =>{
    if(error) throw error;
    console.log("Connect Successfully !");
})

app.use("/",userRouter);
app.use("/",diceHistoryRouter);
app.use("/",prizeRouter);
app.use("/",voucherRouter);

app.listen(port , () =>{
    console.log("App listening on port : ", port);
})